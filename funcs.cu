#include "header.h"

using namespace std;

CUDA_HOSTDEV double GetWaterContent(double h, double hmin, double hmax)
{
    if(h > hmax)
        return phi;
    else
        return phi*max(Krmin, (h - hmin)/(hmax - hmin));
}

double GetSpecMoistCap(double h, double hmin, double hmax)
{
    if(h > hmax)
        return 0.0;
    else
        return phi;//*max(Krmin, (h - hmin)/(hmax - hmin));
}

double GetDensity(double temp)
{
    return rhof0 * (1.0 - temp * betaf);
}
