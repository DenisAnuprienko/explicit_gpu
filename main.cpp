#include "header.h"

using namespace std;

int main(int argc, char *argv[])
{
    if(argc != 2){
        printf("Arguments: N\n");
        return 1;
    }
    int N = atoi(argv[1]);
    if(N <= 0){
        printf("Nonpositive N\n");
        return 2;
    }

    Problem P(1e1, N, N);
    P.Init();
    //P.SolveOnCPU();
    P.SolveOnGPU();

    return 0;
}
