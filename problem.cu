#include "header.h"

using namespace std;

#define BLOCK_DIM 16

__global__ void kernel_SetIC(double *H, double *T, const int nx, const int Ny);
__global__ void kernel_ComputeTheta(double *H, double *Theta, const int Nx, const int Ny, const double dy);
__global__ void kernel_ComputeKr(double *H, double *Theta, double *Krx, double *Kry, const int Nx, const int Ny, const double dy);
__global__ void kernel_ComputeFluidFluxes(double *H, double *T, double *qx, double *qy, double *Krx, double *Kry, const int Nx, const int Ny, const double dx, const double dy, const double K);
__global__ void kernel_ComputeThermalFluxes(const double *T, double *ux, double *uy,
                                            const double *qx, const double *qy,
                                            const int Nx, const int Ny,
                                            const double dx, const double dy, const double lam);
__global__ void kernel_UpdateWaterHead(double *H, double *Theta, const double *qx, const double *qy,
                                       const int Nx, const int Ny,
                                       const double dx, const double dy,
                                       const double dt);
__global__ void kernel_UpdateTemperature(double *T, const double *ux, const double *uy,
                                         const int Nx, const int Ny,
                                         const double dx, const double dy,
                                         const double dt);

__device__ double GetWaterContentDev(double h, double hmin, double hmax);
__device__ double GetSpecMoistCapDev(double h, double hmin, double hmax);
__device__ double GetDensityDev(double temp);

void Problem::SetIC_GPU()
{
    dim3 dimBlock(BLOCK_DIM, BLOCK_DIM);
    dim3 dimGrid((Nx+dimBlock.x-1)/dimBlock.x, (Ny+dimBlock.y-1)/dimBlock.y);
    printf("Launching %dx%d blocks of %dx%d threads\n", (Nx+dimBlock.x-1)/dimBlock.x,
           (Ny+dimBlock.y-1)/dimBlock.y, BLOCK_DIM, BLOCK_DIM);
    //cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);
    kernel_SetIC<<<dimGrid,dimBlock>>>(dev_H, dev_T, Nx, Ny);
    cudaError_t err = cudaGetLastError();
    if(err != 0)
        printf("Error %x\n", err);
    //kernel_ComputeTheta<<<dimGrid,dimBlock>>>(Theta, H, Nx, dy);
    //kernel_ComputeKr<<<dimGrid,dimBlock>>>(H, Theta, Krx, Kry, Nx, Ny, dy);
    //kernel_ComputeFluidFluxes<<<dimGrid,dimBlock>>>(H, qx, qy, Krx, Kry, Nx, Ny, dx, dy, D);
    ComputeTheta_GPU();
    ComputeKr_GPU();
    ComputeFluidFluxes_GPU();
}

void Problem::ComputeTheta_GPU()
{
    dim3 dimBlock(BLOCK_DIM, BLOCK_DIM);
    dim3 dimGrid((Nx+dimBlock.x-1)/dimBlock.x, (Ny+dimBlock.y-1)/dimBlock.y);
    kernel_ComputeTheta<<<dimGrid,dimBlock>>>(dev_H, dev_Theta, Nx, Ny, dy);
}

void Problem::ComputeKr_GPU()
{
    dim3 dimBlock(BLOCK_DIM, BLOCK_DIM);
    dim3 dimGrid((Nx+1+dimBlock.x-1)/dimBlock.x, (Ny+1+dimBlock.y-1)/dimBlock.y);
    //kernel_ComputeKr<<<dimGrid,dimBlock>>>(H, Theta, Krx, Kry, Nx, Ny, dy);
    kernel_ComputeKr<<<dimGrid,dimBlock>>>(dev_H, dev_Theta, dev_Krx, dev_Kry, Nx, Ny, dy);
}

void Problem::ComputeFluidFluxes_GPU()
{
//    dim3 dimBlock(BLOCK_DIM, BLOCK_DIM);
//    dim3 dimGrid((Nx+1+dimBlock.x-1)/dimBlock.x, (Ny+1+dimBlock.y-1)/dimBlock.y);
    dim3 ThreadsPerBlock(BLOCK_DIM, BLOCK_DIM);
    //int BlocksX = Nx/ThreadsPerBlock;
    dim3 NumBlock((Nx+1+BLOCK_DIM-1)/BLOCK_DIM, (Ny+1+BLOCK_DIM-1)/BLOCK_DIM);
    //printf("ComputeFluidFluxesGPU: %dx%d blocks of %d^2 threads\n", Nx/BLOCK_DIM, Ny/BLOCK_DIM, BLOCK_DIM);
    kernel_ComputeFluidFluxes<<<NumBlock,ThreadsPerBlock>>>(dev_H, dev_T,
                                                            dev_qx, dev_qy,
                                                            dev_Krx, dev_Kry,
                                                            Nx, Ny, dx, dy, K);
}

void Problem::ComputeThermalFluxes_GPU()
{
    dim3 ThreadsPerBlock(BLOCK_DIM, BLOCK_DIM);
    dim3 NumBlock((Nx+1+BLOCK_DIM-1)/BLOCK_DIM, (Ny+1+BLOCK_DIM-1)/BLOCK_DIM);
    kernel_ComputeThermalFluxes<<<NumBlock,ThreadsPerBlock>>>(dev_T,
                                                            dev_ux, dev_uy,
                                                            dev_qx, dev_qy,
                                                            Nx, Ny, dx, dy, lam);
}

void Problem::UpdateWaterHead_GPU()
{
    dim3 dimBlock(BLOCK_DIM, BLOCK_DIM);
    dim3 dimGrid((Nx+dimBlock.x-1)/dimBlock.x, (Ny+dimBlock.y-1)/dimBlock.y);
    kernel_UpdateWaterHead<<<dimGrid,dimBlock>>>(dev_H, dev_Theta, dev_qx, dev_qy, Nx, Ny, dx, dy, dt);
}

void Problem::UpdateTemperature_GPU()
{
    dim3 dimBlock(BLOCK_DIM, BLOCK_DIM);
    dim3 dimGrid((Nx+dimBlock.x-1)/dimBlock.x, (Ny+dimBlock.y-1)/dimBlock.y);
    kernel_UpdateTemperature<<<dimGrid,dimBlock>>>(dev_T, dev_ux, dev_uy, Nx, Ny, dx, dy, dt);
}

void Problem::SolveOnGPU()
{
    cudaMalloc((void**)&dev_H,     sizeof(double) * Nx*Ny);
    cudaMalloc((void**)&dev_Theta, sizeof(double) * Nx*Ny);
    cudaMalloc((void**)&dev_T, sizeof(double) * Nx*Ny);
    cudaMalloc((void**)&dev_qx,    sizeof(double) * (Nx+1)*Ny);
    cudaMalloc((void**)&dev_qy,    sizeof(double) * Nx*(Ny+1));
    cudaMalloc((void**)&dev_Krx,   sizeof(double) * (Nx+1)*Ny);
    cudaMalloc((void**)&dev_Kry,   sizeof(double) * Nx*(Ny+1));
    cudaMalloc((void**)&dev_ux,    sizeof(double) * (Nx+1)*Ny);
    cudaMalloc((void**)&dev_uy,    sizeof(double) * Nx*(Ny+1));

    // Still needed for VTK saving
    H     = new double[Nx*Ny];
    Theta = new double[Nx*Ny];
    T     = new double[Nx*Ny];
    qx    = new double[(Nx+1)*Ny];
    qy    = new double[Nx*(Ny+1)];
    ux    = new double[(Nx+1)*Ny];
    uy    = new double[Nx*(Ny+1)];

    SetIC_GPU();
    cudaDeviceSynchronize();
    SaveVTK_GPU(respath + "/sol0.vtk");

    clock_t tend, tbeg = clock();

    double t = 0;
    int filenum = 1;
    for(int k = 1; k < Nt; k++){
        t = k*dt;

        ComputeTheta_GPU();
        cudaDeviceSynchronize();
        ComputeKr_GPU();
        cudaDeviceSynchronize();
        ComputeFluidFluxes_GPU();
        cudaDeviceSynchronize();
        UpdateWaterHead_GPU();
        cudaDeviceSynchronize();

        ComputeThermalFluxes_GPU();
        cudaDeviceSynchronize();
        UpdateTemperature_GPU();
        cudaDeviceSynchronize();

        if(k%save_intensity == 0){
            string fpath = respath + "/sol";
            fpath += to_string(filenum);
            fpath += ".vtk";
            cout << "Time step " << k << "(" << filenum << "), T = " << t << endl;
//            SaveVTK_GPU(fpath);
//            filenum++;
        }
    }
    tend = clock();
    cout << "Total GPU computation time: " << static_cast<double>(tend-tbeg) / CLOCKS_PER_SEC << endl;

    SaveVTK_GPU(respath + "/sol_fin_gpu.vtk");

    cudaFree(dev_H);
    cudaFree(dev_Theta);
    cudaFree(dev_qx);
    cudaFree(dev_qy);
    cudaFree(dev_Krx);
    cudaFree(dev_Kry);
    cudaFree(dev_T);
    cudaFree(dev_ux);
    cudaFree(dev_uy);

    delete [] H;
    delete [] Theta;
    delete [] T;
    delete [] qx;
    delete [] qy;
    delete [] ux;
    delete [] uy;
}


__global__ void kernel_SetIC(double *H, double *T, const int Nx, const int Ny)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;


//    if(i >= 0 && i < Nx && j >= 0 && j < Ny){
//        H[i+j*Nx] = 2.0;
//    }

    const double Lx = 10.0;
    const double Ly = 10.0;
    const double dx = Lx/Nx, dy = dx;

//    if(blockIdx.x == 0 && blockIdx.y == 1){
//        printf("Block 0,1: (%d, %d)\n", i, j);
//    }

    double x = (i+0.5)*dx, y = (j+0.5)*dy;
    if(i >= 0 && i < Nx && j >= 0 && j < Ny){
        //if(i*i + j*j < 400)
        if(sqrt((Lx/2.0-x)*(Lx/2.0-x) + (Ly/2.0-y)*(Ly/2.0-y)) < 2)
            H[i+j*Nx] = 30.0;
        else
            H[i+j*Nx] = 2.0;
        //H[i+j*Nx] = 10.0;//blockIdx.x + blockIdx.y * gridDim.x;
//        if(i == 0 && j == 0)
//            printf("blockDim.x = %d, blockDim.y = %d\n", gridDim.x, gridDim.y);
//        if(blockIdx.x + blockIdx.y * gridDim.x == 35)
//            printf("Block 35 has %dx%d threads\n", blockDim.x, blockDim.y);
        T[i+j*Nx] = 0.0;
    }
}

__global__ void kernel_ComputeTheta(double *H, double *Theta, const int Nx, const int Ny, const double dy)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;

    double hmin = j*dy;
    double hmax = hmin + dy;

    if(i >= 0 && i < Nx && j >= 0 && j < Ny)
        Theta[i+j*Nx] = GetWaterContentDev(H[i+j*Nx], hmin, hmax);

    // Straight adaptation of function GetWaterContent()
//    double h = H[i+j*Nx];
//    if(h > hmax)
//        Theta[i+j*Nx] = phi;
//    else
//        Theta[i+j*Nx] = phi*max(Krmin, (h - hmin)/(hmax - hmin));
}

__global__ void kernel_ComputeKr(double *H, double *Theta, double *Krx, double *Kry,
                                 const int Nx, const int Ny, const double dy)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;

    if(i >= 0 && i < Nx+1 && j >= 0 && j < Ny){
        // Set Krx based on upwind cell
        double hleft  = (i==0)  ? Hlft : H[i-1+j*Nx];
        double hright = (i==Nx) ? Hrgt : H[i+j*Nx];

        if(hright > hleft){
            if(i == Nx)
                Krx[i+j*(Nx+1)] = GetWaterContentDev(Hrgt, j*dy, (j+1)*dy) / phi;
            else
                Krx[i+j*(Nx+1)] = Theta[i+j*Nx]/ phi;
        }
        else{
            if(i == 0)
                Krx[i+j*(Nx+1)] = GetWaterContentDev(Hlft, j*dy, (j+1)*dy) / phi;
            else {
                Krx[i+j*(Nx+1)] = Theta[i-1+j*Nx] / phi;
            }
        }
    }

    if(i >= 0 && i < Nx && j >= 0 && j < Ny+1){
        // Set Kry based on upwind cell
        double hupper = (j == Ny) ? Hupr : H[i+j*Nx];
        double hlower = (j == 0)  ? Hlwr : H[i+(j-1)*Nx];
        double hupw   = 0.0;
        if(hupper > hlower){
            hupw = hupper;
            if(j == Ny)
                Kry[i+j*Nx] = GetWaterContentDev(Hupr, Ny*dy, (Ny-1)*dy) / phi;
            else
                Kry[i+j*Nx] = Theta[i+j*Nx] / phi;
        }
        else{
            if(j == 0)
                Kry[i+j*Nx] = GetWaterContentDev(Hlwr, 0, dy) / phi;
            else
                Kry[i+j*Nx] = Theta[i+(j-1)*Nx] / phi;
        }
    }
}

__global__ void kernel_ComputeFluidFluxes(double *H, double *T, double *qx, double *qy, double *Krx, double *Kry,
                                     const int Nx, const int Ny, const double dx, const double dy,
                                     const double K)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
//    int i = blockIdx.x;
//    int j = blockIdx.y;
    //printf("Fluxes (%d, %d)\n", i, j);

//    if(i >= 0 && i <= Nx && j >= 0 && j < Ny)
//        qx[i+j*(Nx+1)] = 111;

    if(j >= 0 && j < Ny){
        if(i == 0)
            qx[0+j*(Nx+1)]  = 0.0;//-K * Krx[0+j*(Nx+1)]  * (H[0+j*Nx] - Hlft) / (0.5*dx);
        else if(i == Nx){
//            // Seepage
//            double y = (j+0.5)*dy;
//            if(y <= 2.0)
//                 qx[Nx+j*(Nx+1)] = -K * Krx[Nx+j*(Nx+1)] * (Hrgt - H[Nx-1+j*Nx]) / (0.5*dx);
//            else{
//                // Seepage condition
//                if(H[Nx-1+j*Nx] > y)
//                    qx[Nx+j*(Nx+1)] = -K * Krx[Nx+j*(Nx+1)] * (Hrgt - y) / (0.5*dx);
//                else{
//                    double flux = -K * Krx[Nx+j*(Nx+1)] * (Hrgt - H[Nx-1+j*Nx]) / (0.5*dx);
//                    if(flux > 0.0)
//                        qx[Nx+j*(Nx+1)] = 0.0;
//                    else
//                        qx[Nx+j*(Nx+1)] = flux;
//                }
//            }

            qx[Nx+j*(Nx+1)] = 0.0;
        }
        else if(i > 0 && i < Nx)
            qx[i+j*(Nx+1)] = -K * Krx[i+j*(Nx+1)] * (H[i+j*Nx] - H[i-1+j*Nx]) / dx;
    }

    if(i >= 0 && i < Nx){
        if(j == 0)
            qy[i+0*Nx]  = 0.0;//-D * Kry(i,0)  * (H(i,0) - Hlwr) / (0.5*dy);
        else if(j == Ny)
            qy[i+Ny*Nx] = 0.0;//-D * Krx(i,Ny) * (Hupr - H(i,Ny-1)) / (0.5*dy);
        else if(j > 0 && j < Ny){
            qy[i+j*Nx]     = -K * Kry[i+j*Nx]     * (H[i+j*Nx] - H[i+(j-1)*Nx]) / dy;
            double Tupw = min(T[i+j*Nx], T[i+(j-1)*Nx]);
            double rho = GetDensityDev(Tupw);
            qy[i+j*Nx] += -K * Kry[i+j*Nx] * g * (rho - rhof0) / rhof0;
        }
    }
}

__global__ void kernel_ComputeThermalFluxes(const double *T, double *ux, double *uy,
                                            const double *qx, const double *qy,
                                            const int Nx, const int Ny,
                                            const double dx, const double dy, const double lam)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;

    if(j >= 0 && j < Ny){
        if(i == 0){
            ux[0+j*(Nx+1)]  = 0.0;
        }
        else if(i == Nx){
            ux[Nx+j*(Nx+1)]  = 0.0;
        }
        else if(i > 0 && i < Nx){ // Internal faces
            ux[i+j*(Nx+1)] = -lam * (T[i+j*Nx] - T[i-1+j*Nx]) / dx; // Diffusive part
            double Tupw, fflux = qx[i+j*(Nx+1)];
            if(fflux < 0.0)
                Tupw = T[i+j*Nx];
            else
                Tupw = T[i-1+j*Nx];
            ux[i+j*(Nx+1)] += fflux * Tupw;
        }
    }

    if(i >= 0 && i < Nx){
        if(j == 0){       // Lower BC
            uy[i+0*Nx]  = -lam * (T[i+0*Nx] - Tlwr) / (0.5*dy);
            double Tupw, fflux = qy[i+0*Nx];
            if(fflux < 0.0)
                Tupw = T[i+0*Nx];
            else
                Tupw = Tlwr;
            uy[i+0*Nx] += fflux * Tupw;
        }
        else if(j == Ny){ // Upper BC
            uy[i+Ny*Nx] = 0.0;
        }
        else if(j > 0 && j < Ny){ // Internal faces
            uy[i+j*Nx] = -lam * (T[i+j*Nx] - T[i+(j-1)*Nx]) / dy;
            double Tupw, fflux = qy[i+j*Nx];
            if(fflux < 0.0)
                Tupw = T[i+j*Nx];
            else
                Tupw = T[i+(j-1)*Nx];
            uy[i+j*Nx] += fflux * Tupw;
        }
    }
}

__global__ void kernel_UpdateWaterHead(double *H, double *Theta, const double *qx, const double *qy,
                                       const int Nx, const int Ny,
                                       const double dx, const double dy,
                                       const double dt)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    double ctheta = GetSpecMoistCapDev(H[i+j*Nx], j*dy, (j+1)*dy);
    if(i >= 0 && i < Nx && j >= 0 && j < Ny)
        H[i+j*Nx] -= dt * ((qx[i+1+j*(Nx+1)] - qx[i+j*(Nx+1)])/dx +
                           (qy[i+(j+1)*Nx] - qy[i+j*Nx])/dy)
                         /(ctheta + Theta[i+j*Nx]/phi*sstor);
}

__global__ void kernel_UpdateTemperature(double *T, const double *ux, const double *uy,
                                         const int Nx, const int Ny,
                                         const double dx, const double dy,
                                         const double dt)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    if(i >= 0 && i < Nx && j >= 0 && j < Ny)
        T[i+j*Nx] -= dt * ((ux[i+1+j*(Nx+1)] - ux[i+j*(Nx+1)])/dx +
                           (uy[i+(j+1)*Nx]   - uy[i+j*Nx])/dy);
}

__device__ double GetWaterContentDev(double h, double hmin, double hmax)
{
    //return phi;
    if(h > hmax)
        return phi;
    else
        return phi*max(Krmin, (h - hmin)/(hmax - hmin));
}

__device__ double GetSpecMoistCapDev(double h, double hmin, double hmax)
{
    if(h > hmax)
        return 0.0;
    else
        return phi;//*max(Krmin, (h - hmin)/(hmax - hmin));
}

__device__ double GetDensityDev(double temp)
{
    return rhof0 * (1.0 - temp * betaf);
}


void Problem::SaveVTK_GPU(std::string path)
{
    // Copy data from device and perform standard SaveVTK

    cudaMemcpy(H, dev_H, sizeof(double) * Nx*Ny, cudaMemcpyDeviceToHost);
    cudaMemcpy(Theta, dev_Theta, sizeof(double) * Nx*Ny, cudaMemcpyDeviceToHost);
    cudaMemcpy(T, dev_T, sizeof(double) * Nx*Ny, cudaMemcpyDeviceToHost);
    cudaMemcpy(qx, dev_qx, sizeof(double) * (Nx+1)*Ny, cudaMemcpyDeviceToHost);
    cudaMemcpy(qy, dev_qy, sizeof(double) * Nx*(Ny+1), cudaMemcpyDeviceToHost);
    cudaMemcpy(ux, dev_ux, sizeof(double) * (Nx+1)*Ny, cudaMemcpyDeviceToHost);
    cudaMemcpy(uy, dev_uy, sizeof(double) * Nx*(Ny+1), cudaMemcpyDeviceToHost);

    SaveVTK(path);
}
