#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <cmath>

#ifdef __CUDACC__
#define CUDA_HOSTDEV __host__ __device__
#else
#define CUDA_HOSTDEV
#endif

const double K     = 1.0;  // Hydraulic conductivity
const double phi   = 0.4;  // Porosity
const double Krmin = 1e-3; // Minimal relative permeability
//const double thmin = 1e-2;
const double sstor = 1e0;  // Specific storage coefficient
const double rhof0 = 1.0;  // Reference fluid density
const double betaf = 1e-1; // Thermal expansion coefficient
const double lam   = 1e0;  // Heat conductivity
const double g     = 1e2;  // Gravitational acceleration

// Water head BCs
const double Hupr  = 1.0;
const double Hlwr  = 1.0;
const double Hlft  = 10.0;
const double Hrgt  = 2.0;

// Temperature BCs
const double Tupr  = 0.0;
const double Tlwr  = 1.0;
const double Tlft  = 0.0;
const double Trgt  = 0.0;


class Problem
{
private:
    // Domain parameters
    double Lx, Ly;
    double Time;
    // Numerical parameters
    int Nx, Ny; // number of cells
    double dx, dy;
    double dt, dthydro, dtthermo;
    int Nt;
    int save_intensity;

    // Unknowns
    double *H;         // Hydraulic water head
    double *Theta;     // Volumetric water content in porous medium
    double *T;         // Temperature
    double *qx, *qy;   // Fluid fluxes
    double *ux, *uy;   // Thermal fluxes
    double *Krx, *Kry; // Relative permeabilities for water
    // Unknowns on GPU
    double *dev_H;
    double *dev_Theta;
    double *dev_T;
    double *dev_qx, *dev_qy;
    double *dev_ux, *dev_uy;
    double *dev_Krx, *dev_Kry;

    std::string respath;

    // Functions calculating unknowns over whole mesh
    void SetIC();
    void ComputeTheta();
    void ComputeKr();
    void ComputeFluidFluxes();
    void ComputeThermalFluxes();
    void UpdateWaterHead();
    void UpdateTemperature();

    void SetIC_GPU();
    void ComputeTheta_GPU();
    void ComputeKr_GPU();
    void ComputeFluidFluxes_GPU();
    void ComputeThermalFluxes_GPU();
    void UpdateWaterHead_GPU();
    void UpdateTemperature_GPU();

public:
    Problem(double T_, int Nx_, int Ny_) : Time(T_), Nx(Nx_), Ny(Ny_){}
    ~Problem(){}
    void Init(void);
    void SolveOnGPU(void);
    void SolveOnCPU(void);
    void SaveVTK(std::string path);
    void SaveVTK_GPU(std::string path);
};


// Functions calculating some physical values
CUDA_HOSTDEV double GetWaterContent(double h, double hmin, double hmax);
CUDA_HOSTDEV double GetSpecMoistCap(double h, double hmin, double hmax);
CUDA_HOSTDEV double GetDensity(double temp);

#endif // HEADER_H
