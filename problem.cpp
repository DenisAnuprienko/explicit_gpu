#include "header.h"

using namespace std;

void Problem::Init()
{
    Lx = Ly = 10.0;
    dx = Lx / Nx;
    dy = Ly / Ny;
    dthydro  = min(dx*dx, dy*dy) / K * sstor / 16.1; // Time step for explicit Euler
    dtthermo = min(dx*dx, dy*dy) / lam / 16.1; // Time step for explicit Euler
    dt       = min(dthydro, dtthermo) / 1e0;
    cout << "dt = " << dt << endl;
    Nt = static_cast<int>(Time/dt);
    cout << "Nt = " << Nt << endl;
    cout << "sid " << 0.1/dt << endl;
    save_intensity = static_cast<int>(0.5/dt); // saved files have 4.0 between them
    cout << "Save files every " << save_intensity << " step" << endl;
    respath = "/home/denis/parallel/CUDA/res";
}

void Problem::SetIC()
{
    for(int i = 0; i < Nx; i++){
        for(int j = 0; j < Nx; j++){
            double x = (i+0.5)*dx, y = (j+0.5)*dx;
            if(sqrt((Lx/2.0-x)*(Lx/2.0-x) + (Ly/2.0-y)*(Ly/2.0-y)) < 2)
                H[i+j*Nx] = 30.0;
            else
                H[i+j*Nx] = 2.0;

//            if(sqrt((Lx/2.0-x)*(Lx/2.0-x) + (Ly/2.0-y)*(Ly/2.0-y)) < 1)
//                T[i+j*Nx] = 10.0;
//            else
                T[i+j*Nx] = 0.0;
        }
    }
    ComputeTheta();
    ComputeFluidFluxes();
    ComputeThermalFluxes();
}

void Problem::ComputeTheta()
{
    for(int j = 0; j < Ny; j++){
        for(int i = 0; i < Nx; i++){
            double hmin = j*dy;
            double hmax = hmin + dy;
            Theta[i+j*Nx] = GetWaterContent(H[i+j*Nx], hmin, hmax);
        }
    }
}

void Problem::ComputeKr()
{
    for(int j = 0; j < Ny; j++){
        for(int i = 0; i < Nx+1; i++){
            // Set Kr based on upwind cell
            double hleft  = (i==0)  ? Hlft : H[i-1+j*Nx];
            double hright = (i==Nx) ? Hrgt : H[i+j*Nx];

            if(hright > hleft){
                if(i == Nx)
                    Krx[i+j*(Nx+1)] = GetWaterContent(Hrgt, j*dy, (j+1)*dy) / phi;
                else
                    Krx[i+j*(Nx+1)] = Theta[i+j*Nx]/ phi;
            }
            else{
                if(i == 0)
                    Krx[i+j*(Nx+1)] = GetWaterContent(Hlft, j*dy, (j+1)*dy) / phi;
                else {
                    Krx[i+j*(Nx+1)] = Theta[i-1+j*Nx] / phi;
                }
            }
        }
    }
    for(int i = 0; i < Nx; i++){
        for(int j = 0; j < Ny+1; j++){
            // Set Kr based on upper cell
            double hupper = (j == Ny) ? Hupr : H[i+j*Nx];
            double hlower = (j == 0)  ? Hlwr : H[i+(j-1)*Nx];
            double hupw   = 0.0;
            if(hupper > hlower){
                hupw = hupper;
                if(j == Ny)
                    Kry[i+j*Nx] = GetWaterContent(Hupr, Ny*dy, (Ny-1)*dy) / phi;
                else
                    Kry[i+j*Nx] = Theta[i+j*Nx] / phi;
            }
            else{
                if(j == 0)
                    Kry[i+j*Nx] = GetWaterContent(Hlwr, 0, dy) / phi;
                else
                    Kry[i+j*Nx] = Theta[i+(j-1)*Nx] / phi;
            }
        }
    }
}

void Problem::ComputeFluidFluxes()
{
    // Compute fluxes at internal faces
    for(int i = 1; i < Nx; i++){
        for(int j = 0; j < Ny; j++){
            qx[i+j*(Nx+1)] = -K * Krx[i+j*(Nx+1)] * (H[i+j*Nx] - H[i-1+j*Nx]) / dx;
        }
    }
    for(int i = 0; i < Nx; i++){
        for(int j = 1; j < Ny; j++){
            qy[i+j*Nx] = -K * Kry[i+j*Nx] * (H[i+j*Nx] - H[i+(j-1)*Nx]) / dy;
            // Now add density-driven part
            //double Tav = 0.5 * (T[i+j*Nx] + T[i+(j-1)*Nx]);
            double Tupw = min(T[i+j*Nx], T[i+(j-1)*Nx]);
            double rho = GetDensity(Tupw);
            qy[i+j*Nx] += -K * Kry[i+j*Nx] * g * (rho - rhof0) / rhof0;
        }
    }

    // Employ BC
    // x-BC
    for(int j = 0; j < Ny; j++){
        qx[0+j*(Nx+1)]  = 0.0;//-D * Krx[0+j*(Nx+1)]  * (H[0+j*Nx] - Hlft) / (0.5*dx);
//            if(j*dy < 2.0)
            qx[Nx+j*(Nx+1)] = 0.0;//-D * Krx[Nx+j*(Nx+1)] * (Hrgt - H[Nx-1+j*Nx]) / (0.5*dx);
//            else
//                qx(Nx,j) = 0.0;
    }
    // y-BC
    for(int i = 0; i < Nx; i++){
        qy[i+0*Nx]  = 0.0;//-D * Kry(i,0)  * (H(i,0) - Hlwr) / (0.5*dy);
        qy[i+Ny*Nx] = 0.0;//-D * Krx(i,Ny) * (Hupr - H(i,Ny-1)) / (0.5*dy);
    }
}

void Problem::ComputeThermalFluxes()
{
    // Compute fluxes at internal faces
    for(int i = 1; i < Nx; i++){
        for(int j = 0; j < Ny; j++){
            ux[i+j*(Nx+1)] = -lam * (T[i+j*Nx] - T[i-1+j*Nx]) / dx; // Diffusive part
            double Tupw, fflux = qx[i+j*(Nx+1)];
            if(fflux < 0.0)
                Tupw = T[i+j*Nx];
            else
                Tupw = T[i-1+j*Nx];
            ux[i+j*(Nx+1)] += fflux * Tupw;
        }
    }
    for(int i = 0; i < Nx; i++){
        for(int j = 1; j < Ny; j++){
            uy[i+j*Nx] = -lam * (T[i+j*Nx] - T[i+(j-1)*Nx]) / dy;
            double Tupw, fflux = qy[i+j*Nx];
            if(fflux < 0.0)
                Tupw = T[i+j*Nx];
            else
                Tupw = T[i+(j-1)*Nx];
            uy[i+j*Nx] += fflux * Tupw;
        }
    }

    // Employ BC, for now only zero-flux
    // x-BC
    for(int j = 0; j < Ny; j++){
        ux[0+j*(Nx+1)]  = 0.0;//-D * Krx[0+j*(Nx+1)]  * (H[0+j*Nx] - Hlft) / (0.5*dx);
        ux[Nx+j*(Nx+1)] = 0.0;//-D * Krx[Nx+j*(Nx+1)] * (Hrgt - H[Nx-1+j*Nx]) / (0.5*dx);
    }
    // y-BC
    for(int i = 0; i < Nx; i++){
        // Lower
        uy[i+0*Nx]  = -lam * (T[i+0*Nx] - Tlwr) / (0.5*dy);
        double Tupw, fflux = qy[i+0*Nx];
        if(fflux < 0.0)
            Tupw = T[i+0*Nx];
        else
            Tupw = Tlwr;
        uy[i+0*Nx] += fflux * Tupw;

        // Upper
        uy[i+Ny*Nx] = 0.0;//-D * Krx(i,Ny) * (Hupr - H(i,Ny-1)) / (0.5*dy);
    }
}

void Problem::UpdateWaterHead()
{
    for(int i = 0; i < Nx; i++){
        for(int j = 0; j < Ny; j++){
            double ctheta = GetSpecMoistCap(H[i+j*Nx], j*dy, (j+1)*dy);
            H[i+j*Nx] -= dt * ((qx[i+1+j*(Nx+1)] - qx[i+j*(Nx+1)])/dx +
                               (qy[i+(j+1)*Nx] - qy[i+j*Nx])/dy)
                    / (ctheta + sstor * Theta[i+j*Nx] / phi);
        }
    }
}

void Problem::UpdateTemperature()
{
    for(int i = 0; i < Nx; i++){
        for(int j = 0; j < Ny; j++){
            T[i+j*Nx] -= dt * ((ux[i+1+j*(Nx+1)] - ux[i+j*(Nx+1)])/dx +
                               (uy[i+(j+1)*Nx] -   uy[i+j*Nx])/dy);
        }
    }
}

void Problem::SolveOnCPU()
{
    H     = new double[Nx*Ny];
    Theta = new double[Nx*Ny];
    T     = new double[Nx*Ny];
    qx    = new double[(Nx+1)*Ny];
    qy    = new double[Nx*(Ny+1)];
    ux    = new double[(Nx+1)*Ny];
    uy    = new double[Nx*(Ny+1)];
    Krx   = new double[(Nx+1)*Ny];
    Kry   = new double[Nx*(Ny+1)];

    SetIC();
    SaveVTK(respath + "/sol0.vtk");

    clock_t tend, tbeg = clock();

    double t = 0;
    int filenum = 1;
    for(int k = 1; k < Nt; k++){
        t = k*dt;

        ComputeTheta();
        ComputeKr();
        ComputeFluidFluxes();
        UpdateWaterHead();

        ComputeThermalFluxes();
        UpdateTemperature();

        if(k%save_intensity == 0){
            string fpath = respath + "/sol";
            fpath += to_string(filenum);
            fpath += ".vtk";
            cout << "Time step " << k << "(" << filenum << "), T = " << t << endl;
            SaveVTK(fpath);
            filenum++;
        }
    }
    tend = clock();
    cout << "Total CPU computation time: " << static_cast<double>(tend-tbeg) / CLOCKS_PER_SEC << endl;

    //SaveVTK(respath + "/sol_fin.vtk");

    delete [] H;
    delete [] Theta;
    delete [] T;
    delete [] qx;
    delete [] qy;
    delete [] Krx;
    delete [] Kry;
    delete [] ux;
    delete [] uy;
}

void Problem::SaveVTK(string path)
{
    ofstream out;
    out.open(path);
    if(!out.is_open()){
        cout << "Couldn't open file " << path << "!";
        exit(0);
    }
    out << "# vtk DataFile Version 3.0" << endl << endl;
    out << "ASCII" << endl;
    out << "DATASET STRUCTURED_GRID" << endl;
    out << "DIMENSIONS " << Nx+1 << " " << Ny+1 << " 1" << endl;
    out << "POINTS " << (Nx+1)*(Ny+1) << " DOUBLE" << endl;

    for(int i = 0; i <= Nx; i++){
        for(int j = 0; j <= Ny; j++){
            out << j*dy << " " << i*dx << " 0.0" << endl;
        }
    }

    out << "CELL_DATA " << Nx * Ny << endl;

    out << "SCALARS Water_Head double" << endl;
    out << "LOOKUP_TABLE default" << endl;
    for(int j = 0; j < Ny; j++){
        for(int i = 0; i < Nx; i++){
            out << H[i+j*Nx] << endl;
        }
    }

    out << "SCALARS Water_Content double" << endl;
    out << "LOOKUP_TABLE default" << endl;
    for(int j = 0; j < Ny; j++){
        for(int i = 0; i < Nx; i++){
            out << Theta[i+j*Nx] << endl;
        }
    }

    out << "SCALARS Temperature double" << endl;
    out << "LOOKUP_TABLE default" << endl;
    for(int j = 0; j < Ny; j++){
        for(int i = 0; i < Nx; i++){
            out << T[i+j*Nx] << endl;
            //out << GetDensity(T[i+j*Nx]) << endl;
        }
    }

//    out << "SCALARS Kr_average double" << endl;
//    out << "LOOKUP_TABLE default" << endl;
//    for(int j = 0; j < Ny; j++){
//        for(int i = 0; i < Nx; i++){
//            out << 0.25*(Krx(i,j)+Kry(i,j)+Krx(i+1,j)+Kry(i,j+1)) << endl;
//        }
//    }

//    out << "SCALARS Ctheta double" << endl;
//    out << "LOOKUP_TABLE default" << endl;
//    for(int j = 0; j < Ny; j++){
//        for(int i = 0; i < Nx; i++){
//            out << get_spec_moist_cap(H(i,j), j*dy, j*dy+dy) << endl;
//        }
//    }

    out << "VECTORS FluidFlux double" << endl;
    for(int j = 0; j < Ny; j++){
        for(int i = 0; i < Nx; i++){
            out << qx[i+j*(Nx+1)] << " " << qy[i+j*Nx] << " 0.0" << endl;
        }
    }

    out << "VECTORS ThermalFlux double" << endl;
    for(int j = 0; j < Ny; j++){
        for(int i = 0; i < Nx; i++){
            out << ux[i+j*(Nx+1)] << " " << uy[i+j*Nx] << " 0.0" << endl;
        }
    }

    out.close();
}
